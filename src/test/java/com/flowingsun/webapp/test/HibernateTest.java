package com.flowingsun.webapp.test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.flowingsun.webapp.dao.CanteenDao;
import com.flowingsun.webapp.dao.MealMenuDao;
import com.flowingsun.webapp.domain.MealMenu;
import com.flowingsun.webapp.domain.MealPackage;
import com.flowingsun.webapp.service.CanteenService;

import junit.framework.Assert;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml", "classpath:spring-hibernate.xml" })
public class HibernateTest {

	@Autowired
	private MealMenuDao mealMenuDao;

	@Autowired
	private CanteenService canteenService;

	@Test
	public void testPoToVo() {
		for (int i = 0; i < 20; i++) {// 40 times in jam?
			MealMenu mm = mealMenuDao.FindEnityById(1L);
			System.out.println(mm.getMealPackages().size() + "");
		}
		Assert.assertTrue(true);
	}

	@Test
	public void testLambda() {
		List<MealPackage> set = canteenService.canteenTestHibernateLazy();
		List<MealPackage> setNew =filterMP(set,(MealPackage a)->a.getState()==1);
		for (MealPackage mealPackage : setNew) {
			System.out.println(mealPackage.getPackageName());
		}
	}

	// static <T> Collection<T> filter(Collection<T> c,Predicate<T> p);

	static List<MealPackage> filterMP(List<MealPackage> i, Predicate<MealPackage> p){
		List<MealPackage> result = new ArrayList<MealPackage>();
		for (MealPackage mealPackage : result) {
			if(p.test(mealPackage)){
				result.add(mealPackage);
			}
		}
		return result;
	}
}
