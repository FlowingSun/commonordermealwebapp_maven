Lucene = {};
Lucene.initSearchForms = function(obj,index,cmpSettings) {
	var lucItem = {};
	
	if(cmpSettings){
		lucItem=cmpSettings;
		lucItem.xtype='textfield';
		lucItem.fieldLabel=cmpSettings.fieldLabel?cmpSettings.fieldLabel:"模糊查询";
		lucItem.margin=cmpSettings.margin?cmpSettings.margin:'10 5 10 0';
		lucItem.name=cmpSettings.name?cmpSettings.name:'luceneSearch';
	}
	
	obj.prototype.items.splice(index, 0, lucItem);
}
