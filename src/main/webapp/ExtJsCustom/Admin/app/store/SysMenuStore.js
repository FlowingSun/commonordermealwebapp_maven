Ext.define("OrderMealAdmin.store.SysMenuStore",{
	extend:'Ext.data.TreeStore',
	storeId: 'SysMenuStore',
	defaultRootId:'root',
	proxy:{
		type:'ajax',
		url:'/CommonOrderMealWebApp_Maven/admin/GetAllSysMenu.html',
		reader:{
			type:'json'
		},
		autoLoad:true
	}
	
});