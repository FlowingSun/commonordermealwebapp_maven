Ext.define('OrderMealAdmin.view.OrderManage.OrderManageGrid', {
	extend : 'Ext.grid.Panel',
	alias : 'widget.OrderManageGrid',
	store : 'OrderManageStore',
//	height : 540,
//	width : 500,
	height : '100%',
	width : '100%',
	selType : 'checkboxmodel',
	multiSelect : true,
	columnLines : true,// 表格的竖线
//	scrollable : true,
	initComppnent : function() {
		console.log(arguments);
		this.callParent(arguments);
	},
	border : 0,
	columns : [ {
		text : 'id',
		dataIndex : 'mealOrderId',
		width : 30,
		locked : true
	}, {
		text : '套餐名称',
		dataIndex : 'mealPackageName',
		width : 140,
		locked : true
	}, {
		text : '订餐人',
		dataIndex : 'userName',
		width : 140,
		locked : true
	}, {
		text : '套餐价格',
		dataIndex : 'mealPackagePrice',
		width : 140,
		locked : true
	}, {
		text : '订餐位置',
		dataIndex : 'postion',
		width : 140
	}, {
		text : '订餐人联系电话',
		dataIndex : 'telePhone',
		width : 140
	}, {
		text : '用户所在位置',
		dataIndex : 'userLocation',
		width : 140
	}, {
		text : '订餐时间',
		dataIndex : 'orderTime',
		width : 140
	}, {
		text : '套餐供应时段',
		dataIndex : 'supplyTimeType',
		width : 140
	}, {
		text : '套餐开始供应时间',
		dataIndex : 'beginSupplyTime',
		width : 140
	}, {
		text : '套餐结束供应时间',
		dataIndex : 'endSupplyTime',
		width : 140
	}, {
		text : '套餐开始供应日期',
		dataIndex : 'beginSupplyDate',
		width : 140
	}, {
		text : '套餐开始供应日期',
		dataIndex : 'endSupplyDate',
		width : 140
	}, {
		text : '套餐所属菜单',
		dataIndex : 'mealMenuName',
		width : 140
	}, {
		text : '订单状态',
		dataIndex : 'orderState',
		width : 150,
		xtype : 'booleancolumn',
		trueText : "<font color='green'>正常</font>",
		falseText : "<font color='red'>已取消</font>"
	} ],
	tbar : [ {
		xtype : 'button',
		text : '添加',
		icon : 'ExtJsCustom/images/user_add.png'
	}, {
		xtype : 'button',
		text : '修改',
		icon : 'ExtJsCustom/images/user_edit.png',
		disabled : true
	}, {
		xtype : 'button',
		text : '停用',
		icon : 'ExtJsCustom/images/delete.gif',
		disabled : true
	} ],
	dockedItems : [ {
		xtype : 'pagingtoolbar',
		store : 'OrderManageStore',
		displayInfo : true,
		dock : 'bottom'
	} ]
});