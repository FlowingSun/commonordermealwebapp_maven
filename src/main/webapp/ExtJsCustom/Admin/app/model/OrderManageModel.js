Ext.define('OrderMealAdmin.model.OrderManageModel',{
	extend:'Ext.data.Model',
	fields:[
		{name:'mealOrderId',mapping:'mealOrderId',type:'int'},
		{name:'orderState',mapping:'orderState',type:'int'},
		{name:'userId',mapping:'userId',type:'int'},
		{name:'userName',mapping:'userName',type:'string'},
		{name:'postion',mapping:'postion',type:'string'},
		{name:'telePhone',mapping:'telePhone',type:'string'},
		{name:'userLocation',mapping:'userLocation',type:'string'},
		{name:'orderTime',mapping:'orderTime',type:'time',dataFormat:'YYYY-MM-dd HH:mm:ss'},
		{name:'canteenName',mapping:'canteenName',type:'string'},
		{name:'canteenId',mapping:'canteenId',type:'int'},
		{name:'packageName',mapping:'packageName',type:'string'},
		{name:'supplyTimeType',mapping:'supplyTimeType',type:'string'},
		{name:'beginSupplyTime',mapping:'beginSupplyTime',type:'time',dataFormat:'HH:mm:ss'},
		{name:'endSupplyTime',mapping:'endSupplyTime',type:'time',dataFormat:'HH:mm:ss'},
		{name:'beginSupplyDate',mapping:'beginSupplyDate',type:'time',dataFormat:'YYYY-MM-dd'},
		{name:'endSupplyDate',mapping:'endSupplyDate',type:'time',dataFormat:'YYYY-MM-dd'},
		{name:'mealMenuId',mapping:'mealMenuId',type:'int'},
		{name:'mealMenuName',mapping:'mealMenuName',type:'string'},
		{name:'mealPackageId',mapping:'mealPackageId',type:'int'},
		{name:'mealPackageName',mapping:'mealPackageName',type:'string'},
		{name:'mealPackagePrice',mapping:'mealPackagePrice',type:'string'}
		/*{name:'description',mapping:'description',type:'string'},
		{name:'editTime',mapping:'editTime',type:'time',dataFormat:'YYYY-MM-dd HH:mm:ss'},*/
	]
});