Ext.define('OrderMealAdmin.model.IndexNavigationModel',{
	extend:'Ext.data.Model',
	fields:[
		{name:'id',mapping:'id',type:'int'},
		{name:'name',mapping:'name',type:'string'},
		{name:'state',mapping:'state',type:'int'},
		{name:'mainTitle',mapping:'mainTitle',type:'string'},
		{name:'subTitle',mapping:'subTitle',type:'string'},
		{name:'description',mapping:'description',type:'string'},
		{name:'imageUrl',mapping:'imageUrl',type:'string'},
		{name:'createTime',mapping:'createTime',type:'time',dataFormat:'YYYY-MM-dd HH:mm:ss'},
		{name:'editTime',mapping:'editTime',type:'time',dataFormat:'YYYY-MM-dd HH:mm:ss'},
	]
});