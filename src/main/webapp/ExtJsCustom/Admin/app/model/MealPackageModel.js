Ext.define('OrderMealAdmin.model.MealPackageModel',{
	extend:'Ext.data.Model',
	fields:[
		{name:'mealPackageId',mapping:'mealPackageId',type:'int'},
		{name:'canteenId',mapping:'canteenId',type:'int'},
		{name:'mealMenuId',mapping:'mealMenuId',type:'int'},
		{name:'packageName',mapping:'packageName',type:'string'},
		{name:'mealTimeType',mapping:'mealTimeType',type:'string'},
		{name:'packagePrice',mapping:'packagePrice',type:'string'},
		{name:'state',mapping:'state',type:'int'},
		{name:'description',mapping:'description',type:'string'},
		{name:'createTime',mapping:'createTime',type:'time',dataFormat:'YYYY-MM-dd HH:mm:ss'},
		{name:'editTime',mapping:'editTime',type:'time',dataFormat:'YYYY-MM-dd HH:mm:ss'},
	]
});