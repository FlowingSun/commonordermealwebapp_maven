Ext.define('OrderMealAdmin.model.MealPackageUserMarkModel',{
	extend:'Ext.data.Model',
	fields:[
		{name:'mealPackageUserMarkId',mapping:'mealPackageUserMarkId',type:'int'},
		{name:'markName',mapping:'markName',type:'string'},
		{name:'state',mapping:'state',type:'int'},
		{name:'description',mapping:'description',type:'string'},
		{name:'createTime',mapping:'createTime',type:'time',dataFormat:'YYYY-MM-dd HH:mm:ss'},
		{name:'editTime',mapping:'editTime',type:'time',dataFormat:'YYYY-MM-dd HH:mm:ss'},
	]
});