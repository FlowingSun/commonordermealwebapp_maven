package com.flowingsun.webapp.controller;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.flowingsun.webapp.domain.ExtjsPaging;
import com.flowingsun.webapp.service.MealOrderService;

@Controller
@RequestMapping("/admin/OrderSummaryManage")
public class SysAdmin_OrderSummaryManageController {

	@Autowired
	private MealOrderService mealOrderService;

	@RequestMapping("/GetPagingOrderSummaries")
	@ResponseBody
	public Object GetPagingEntities(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("ExtjsPaging") ExtjsPaging model) throws Exception {
		return null;
	}

}
