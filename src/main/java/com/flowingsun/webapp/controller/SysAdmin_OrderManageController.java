package com.flowingsun.webapp.controller;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.flowingsun.webapp.domain.ExtjsPaging;
import com.flowingsun.webapp.domain.FormMessage;
import com.flowingsun.webapp.domain.MealOrder;
import com.flowingsun.webapp.domain.MealPackageUserMark;
import com.flowingsun.webapp.service.MealOrderService;
import com.flowingsun.webapp.service.MealPackageService;

@Controller
@RequestMapping("/admin/OrderManage")
public class SysAdmin_OrderManageController {

	@Autowired
	private MealOrderService mealOrderService;

	@RequestMapping("/GetPagingOrderManages")
	@ResponseBody
	public Object GetPagingEntities(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("ExtjsPaging") ExtjsPaging model) throws Exception {
		Map<String, Object> params = new HashMap<String, Object>();
		Iterator<Entry<String, String[]>> iterator = request.getParameterMap().entrySet().iterator();
		while (iterator.hasNext()) {
			Map.Entry<String, String[]> entry = (Map.Entry<String, String[]>) iterator.next();
			String key = entry.getKey();
			String val = entry.getValue()[0];
			params.put(key, val);
		}
		List<MealOrder> list = mealOrderService.GetPagingEntities(params, model.getPage(),
				model.getPageSize());
		return list;
	}

	@RequestMapping("/addMealPackage")
	@ResponseBody
	public Object AddEntity(HttpServletRequest request, MealOrder mealOrder) {
		FormMessage result = new FormMessage();
		try {
			mealOrderService.Save(mealOrder);
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setMsg(e.getMessage());
			return result;
		}
		result.setSuccess(true);
		result.setMsg("添加成功");
		return result;
	}

	@RequestMapping("/updateMealPackage")
	@ResponseBody
	public Object UpdateEntity(HttpServletRequest request, MealOrder mealOrder) {
		FormMessage result = new FormMessage();
		try {
			mealOrderService.Update(mealOrder);
		} catch (Exception e) {
			e.printStackTrace();
			result.setSuccess(false);
			result.setMsg(e.getMessage());
			return result;
		}
		result.setSuccess(true);
		result.setMsg("修改成功");
		return result;
	}
}
