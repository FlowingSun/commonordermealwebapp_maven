package com.flowingsun.webapp.dao;

import com.flowingsun.webapp.dao.common.BaseDao;
import com.flowingsun.webapp.domain.MealPackageUserMark;

public interface MealPackageUserMarkDao extends BaseDao<MealPackageUserMark, Long> {

}
