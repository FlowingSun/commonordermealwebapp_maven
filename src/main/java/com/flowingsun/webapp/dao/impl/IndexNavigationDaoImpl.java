package com.flowingsun.webapp.dao.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.hibernate.criterion.MatchMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.flowingsun.webapp.dao.IndexNavigationDao;
import com.flowingsun.webapp.dao.common.CommonDao;
import com.flowingsun.webapp.dao.common.HSerchEntity;
import com.flowingsun.webapp.domain.IndexNavigation;
import com.flowingsun.webapp.domain.MealMenu;

@Repository
public class IndexNavigationDaoImpl implements IndexNavigationDao {

	@Autowired
	private CommonDao<IndexNavigation> commonDao;

	@Override
	public IndexNavigation FindEnityById(Long Id) {
		return commonDao.LoadEntityById(IndexNavigation.class, Id, "IndexNavigationId");
	}

	@Override
	public void Update(IndexNavigation entity) {
		commonDao.UpdateEntity(entity);
	}

	@Override
	public void Save(IndexNavigation entity) {
		commonDao.SaveEntity(entity);
	}

	@Override
	public void ChangeState(int id, int state, String stateColumn) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<IndexNavigation> GetPagingEntities(Map<String, Object> params, int page, int pageSize) {
		List<HSerchEntity> hses = new ArrayList<HSerchEntity>();
		Iterator<Entry<String, Object>> iterator = params.entrySet().iterator();
		IndexNavigation entity = new IndexNavigation();// TODO--Spring.getBean
		Map<String, String> fieldMap = entity.GetClassFieldMaps();

		while (iterator.hasNext()) {
			Map.Entry<String, Object> entry = iterator.next();
			String key = entry.getKey();
			String val = (String) entry.getValue();
			if (null == val || val.length() <= 0) {
				continue;
			}
			if (!fieldMap.containsKey(key.toLowerCase())) {
				if (key.toLowerCase().equals("name")) {
					key = "name";
				} else {
					continue;
				}
			} else {
				key = fieldMap.get(key.toLowerCase());
			}
			if (key.toLowerCase().equals("name")) {
				HSerchEntity hse = new HSerchEntity();
				hse.ColumnName = "Name";
				hse.ColumnValue = params.get("Name").toString();
				hse.HMachMode = MatchMode.ANYWHERE;
				hses.add(hse);
			} else {
				HSerchEntity hse = new HSerchEntity();
				hse.ColumnName = key;
				if (key.toLowerCase().equals("id")) {
					hse.ColumnValue = Long.parseLong(val);
				} else {
					hse.ColumnValue = val;
				}
				hse.HMachMode = null;
				hses.add(hse);
			}
		}
		return commonDao.LoadPagingEntities(IndexNavigation.class, hses, page, pageSize);
	}

	@Override
	public List<IndexNavigation> FindAllEntities() {
		// TODO Auto-generated method stub
		return null;
	}

}
