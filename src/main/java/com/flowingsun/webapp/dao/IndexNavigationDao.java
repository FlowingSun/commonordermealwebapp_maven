package com.flowingsun.webapp.dao;

import java.util.List;

import com.flowingsun.webapp.dao.common.BaseDao;
import com.flowingsun.webapp.domain.IndexNavigation;

public interface IndexNavigationDao extends BaseDao<IndexNavigation, Long> {
	public List<IndexNavigation> FindAllEntities();
}
