package com.flowingsun.webapp.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Component
public class MealMenuRole {

	private Long mealMenuRoleId;
	private int roleType;
	private String roleName;
	private String roleContent;
	private MealMenu mealMenu;
	
	/**
	 * @return the mealMenuRoleId
	 */
	@Id
	@GeneratedValue
	@Column(name = "ID", updatable = false)
	public Long getMealMenuRoleId() {
		return mealMenuRoleId;
	}
	/**
	 * @param mealMenuRoleId the mealMenuRoleId to set
	 */
	public void setMealMenuRoleId(Long mealMenuRoleId) {
		this.mealMenuRoleId = mealMenuRoleId;
	}
	/**
	 * @return the roleType
	 */
	public int getRoleType() {
		return roleType;
	}
	/**
	 * @param roleType the roleType to set
	 */
	public void setRoleType(int roleType) {
		this.roleType = roleType;
	}
	/**
	 * @return the roleName
	 */
	public String getRoleName() {
		return roleName;
	}
	/**
	 * @param roleName the roleName to set
	 */
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	/**
	 * @return the roleContent
	 */
	public String getRoleContent() {
		return roleContent;
	}
	/**
	 * @param roleContent the roleContent to set
	 */
	public void setRoleContent(String roleContent) {
		this.roleContent = roleContent;
	}
	/**
	 * @return the mealMenu
	 */
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "MealMenuID")
	public MealMenu getMealMenu() {
		return mealMenu;
	}
	/**
	 * @param mealMenu the mealMenu to set
	 */
	public void setMealMenu(MealMenu mealMenu) {
		this.mealMenu = mealMenu;
	}
}
	