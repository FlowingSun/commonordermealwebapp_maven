package com.flowingsun.webapp.service;

import com.flowingsun.webapp.domain.MealPackage;

public interface MealPackageService extends BaseService<MealPackage> {
}
