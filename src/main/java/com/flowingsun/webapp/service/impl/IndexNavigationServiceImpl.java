package com.flowingsun.webapp.service.impl;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.flowingsun.webapp.dao.IndexNavigationDao;
import com.flowingsun.webapp.domain.IndexNavigation;
import com.flowingsun.webapp.service.IndexNavigationService;

@Service
@Transactional
public class IndexNavigationServiceImpl implements IndexNavigationService {

	@Autowired
	private IndexNavigationDao indexNavigationDao;

	@Override
	public List<IndexNavigation> GetPagingEntities(Map<String, Object> params, int page, int pageSize) {
		return indexNavigationDao.GetPagingEntities(params, page, pageSize);
	}

	@Override
	public void Update(IndexNavigation entity) {
		indexNavigationDao.Update(entity);
	}

	@Override
	public void Save(IndexNavigation entity) throws Exception {
		indexNavigationDao.Save(entity);
	}

	@Override
	public List<IndexNavigation> FindAllEntities() {
		return indexNavigationDao.FindAllEntities();
	}

}
