package com.flowingsun.webapp.service.impl;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.flowingsun.webapp.dao.MealOrderDao;
import com.flowingsun.webapp.domain.MealOrder;
import com.flowingsun.webapp.service.MealOrderService;

@Service
@Transactional
public class MealOrderServiceImpl implements MealOrderService {

	@Autowired
	private MealOrderDao mealOrderDao;
	
	@Override
	public List<MealOrder> GetPagingEntities(Map<String, Object> params, int page, int pageSize) {
		List<MealOrder> list = mealOrderDao.GetPagingEntities(params, page, pageSize);
		for (MealOrder mealOrder : list) {
			mealOrder.getUser().getUserName();
		}
		return list;
	}

	@Override
	public void Update(MealOrder entity) {
		mealOrderDao.Update(entity);
	}

	@Override
	public void Save(MealOrder entity) throws Exception {
		mealOrderDao.Save(entity);
	}

}
