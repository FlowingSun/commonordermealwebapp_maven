package com.flowingsun.webapp.service.impl;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.flowingsun.webapp.dao.MealPackageDao;
import com.flowingsun.webapp.domain.MealPackage;
import com.flowingsun.webapp.service.MealPackageService;

@Service
@Transactional
public class MealPackageServiceImpl implements MealPackageService {

	@Autowired
	private MealPackageDao mealPackageDao;

	@Override
	public List<MealPackage> GetPagingEntities(Map<String, Object> params, int page, int pageSize) {
		return mealPackageDao.GetPagingEntities(params, page, pageSize);
	}

	@Override
	public void Update(MealPackage entity) {
		mealPackageDao.Update(entity);
	}

	@Override
	public void Save(MealPackage entity) throws Exception {
		mealPackageDao.Save(entity);
	}

}
