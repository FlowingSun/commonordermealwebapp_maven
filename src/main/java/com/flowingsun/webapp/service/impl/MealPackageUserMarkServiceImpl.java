package com.flowingsun.webapp.service.impl;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.flowingsun.webapp.dao.MealPackageUserMarkDao;
import com.flowingsun.webapp.domain.MealPackageUserMark;
import com.flowingsun.webapp.service.MealPackageUserMarkService;

@Service
@Transactional
public class MealPackageUserMarkServiceImpl implements MealPackageUserMarkService {
	@Autowired
	private MealPackageUserMarkDao mealPackageUserMarkDao;

	@Override
	public List<MealPackageUserMark> GetPagingEntities(Map<String, Object> params, int page, int pageSize) {
		return mealPackageUserMarkDao.GetPagingEntities(params, page, pageSize);
	}

	@Override
	public void Update(MealPackageUserMark entity) {
		mealPackageUserMarkDao.Update(entity);
	}

	@Override
	public void Save(MealPackageUserMark entity) throws Exception {
		mealPackageUserMarkDao.Save(entity);
	}

}
