package com.flowingsun.webapp.service;

import java.util.List;
import java.util.Map;

import com.flowingsun.webapp.domain.Canteen;
import com.flowingsun.webapp.domain.IndexNavigation;
import com.flowingsun.webapp.domain.User;

public interface IndexNavigationService {
	
	public List<IndexNavigation> GetPagingEntities(Map<String, Object> params, int page,
			int pageSize);

	public void Update(IndexNavigation entity);

	public void Save(IndexNavigation entity) throws Exception;
	
	public List<IndexNavigation> FindAllEntities();
}
