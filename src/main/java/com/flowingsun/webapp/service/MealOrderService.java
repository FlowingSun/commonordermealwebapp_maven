package com.flowingsun.webapp.service;

import com.flowingsun.webapp.domain.MealOrder;

public interface MealOrderService extends BaseService<MealOrder> {

}
