package com.flowingsun.webapp.service;

import java.util.List;
import java.util.Map;


public interface BaseService<T> {
	public List<T> GetPagingEntities(Map<String, Object> params, int page,
			int pageSize);

	public void Update(T entity);

	public void Save(T entity) throws Exception;
}
